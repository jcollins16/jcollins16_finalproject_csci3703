# Final Project For CSCI 3703

This project is a functional product meeting the requirements of the final project for CSCI3703.
It includes physical push buttons, digital buttons, and a voice command system which all affect a webpage. The UI is designed to keep track of the cups of water consumed on a daily basis; a timer feature is also present, which keeps track of the time between activations of the counter, and thus the time elapsed since drinking water.

There are also three LEDs present on the physical portion of the project. A green LED is briefly illuminated when the counter is incremented. A yellow LED turns on when the timer reaches 1800 seconds (or 30.0 minutes), indicating the user will need to drink water soon. A red LED is triggered when the timer reaches 3600 seconds (or 60 minutes), indicating that the user should drink water now. Both the yellow and red LEDs are turned off when the interface is incremented by one. A audio prompt is also played when the timer reaches 1800 seconds, warning the user that its been a while since drinking water. In addition, a audio warning is played when the timer reaches 3600 seconds, warning the user that they should drink water now. 

The project includes physical push buttons which increments the counter by one, and the webpage updates the count. The second push button resets the counter. Both actions are followed by a feedback noise and lights to indicate successful activation. 

In conjunction with the physical push buttons, the digital portion of the interface has html buttons which mimic the actions of the push buttons, including incrementing the counter and resetting the total count. The existance of these digital buttons enables use of the interface when not in close proximity to the physical buttons.

Additionally, this project also includes voice command functionality, which also enables incrementation of the counter, resetting the counter, and the additional feature of reading out the current count displayed on the interface.

## How to use
Navigate into the jcollins16_FinalProject_CSCI3703 repository. Next, navigate into the FinalProject directory.

Use this command to begin the program:
```bash
python3 physical_to_Screen.py
```
Navigate to any common browser and enter the IP address of the Raspberry Pi in the search bar, followed by a :5000 routing address.
```bash
000.000.0.000:5000
```
The counter can be incremented via 3 different interfaces: Physical, Digital, and Voice Commands.

#### Physical
The blue button on the physical interface increments the counter by one, and resets the timer.

The red button on the physical interface resets the total count for the counter.

#### Digital
The digital 'Drink' button increments the counter by one, and resets the timer.

The digital 'Reset' button resets the total count for the counter.

#### Voice Commands
You can increment the counter by saying, out loud:
```bash
Increase count
```
You can also reset the system by saying, out loud:
```bash
Reset
```
You can also have system read back the current count by saying:
```bash
How much water
```

## Hardware

- Raspberry Pi 3 B+;
- Raspiaudio MIC +;
- Breadboard
- (3) colored LEDs
- (2) physical push buttons
- (12) male to female jumper wires


## Hardware Arrangement
The GPIO items are arranged using the standard Broadcom pin number (BCM) scheme.
Pin numbers were chosen to not interfere with pins already in use by the Raspiaudio Pi HAT.

- counterButton = PIN(26)
- button2 = PIN(3)
- ledRed = PIN(4)
- ledYellow = PIN(27)
- ledGreen = PIN(17)


### Built with
- Python3 - Scripting language
- Flask - Web server
- Gpiozero - GPIO manipulation
- W3.css - Styling and responsive web design
-aiy.cloudspeech API - Voice commands

### Author
Created by Jordan Collins for CSCI 3703

