#!/usr/bin/env python3

#imports for voice control
import argparse #Translate speech into text functionality
import locale #Import English language
import logging #Google is watching
from aiy.board import Board, Led
from aiy.cloudspeech import CloudSpeechClient
import aiy.voice.tts

#imports for gpio/sound/threading/Flask tasks
from gpiozero import * 
from time import sleep
import pygame
from gpiozero import Button
from gpiozero import LED
from flask import Flask, render_template, jsonify, request
import time
from concurrent.futures import ThreadPoolExecutor
from signal import pause


app = Flask(__name__)

#initiating a thread just for google voice control
executor = ThreadPoolExecutor(1)

#gpio pin locations based on BCM
counterButton = Button(26) 
button2 = Button(3)
ledRed = LED(4)
ledYellow = LED(27)
ledGreen= LED(17)

#initilizing the sounds played when physical button pressed
pygame.init()
buttonSound = pygame.mixer.Sound("/home/pi/jcollins16_buttontoweb/audio_feedback/samples/buttonClick2.wav")
resetSound = pygame.mixer.Sound("/home/pi/jcollins16_buttontoweb/audio_feedback/samples/resetClick.wav")

start_time = 0

buttonSts = 0

##get_hints gives hints in shell on what voice commands work ##

def get_hints(language_code):
    if language_code.startswith('en_'):
        return ('drink water',
                'reset', 'how much water')
    return None

##Get english##
def locale_language():
    language, _ = locale.getdefaultlocale()
    return language

##Index function enables physical buttons, plays audio sounds on button presses, enables timer functionality, and initilizes flask server##

@app.route("/") 
def index():
    global buttonSts
    global elapsed_time
    global start_time
    
    if counterButton.is_pressed:
        start_time = time.time()                 
        buttonSts+=1
        ledGreen.on()
        buttonSound.play()
        end = time.time()
    else:
        ledGreen.off()
        ledYellow.off()
        ledRed.off()

    if button2.is_pressed:
        buttonSts=0    
        ledRed.on()
        resetSound.play()
    else:
        ledRed.off()
    
    elapsed_time = time.time() - start_time
    formatted_time = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))

#Timer feature that warns you when you need to drink water!
#Python is counting elapsed time in seconds
#Yellow light comes on when its been 1800 seconds; first voice warning
#Red light comes on when its been 3600 seconds; final voice warning
  
    if elapsed_time >= 1800:
        ledYellow.on()

    if 1800 < elapsed_time < 1801:
        aiy.voice.tts.say("It's been a while since you've drank any water")
    
    if elapsed_time >= 3600:
        ledRed.on()
        ledYellow.off()
    
    if 3601 < elapsed_time < 3601:
        aiy.voice.tts.say("Time to drink some water!")
        
    
##Create a thread for function main to run on, to loop/listen continuously in the background##
    executor.submit(voice_to_text)
    
    templateData= {
    'title' : 'Did you drink water today!?',
    'button' : buttonSts,
    'elapsed_time' : formatted_time,
    }
    
    logging.info('rendering template')
    return render_template('index.html', **templateData)


#Extra Flask routes to enable AJAX commands based on digital button presses
@app.route('/DrinkFunction')
def drinkButton():
    global buttonSts
    global elapsed_time
    global start_time
    start_time = time.time()                 
    buttonSts+=1
    ledGreen.on()
    buttonSound.play()
    end = time.time()
    ledYellow.off()
    ledRed.off()
    elapsed_time = time.time() - start_time
    formatted_time = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))

    return render_template('index.html')

@app.route('/ResetFunction')
def resetButton():
    global buttonSts
    buttonSts=0    
    ledRed.on()
    resetSound.play()

    return render_template('index.html')

    
##Function allows voice commands for webpage using aiy.Cloudspeech API##    
def voice_to_text():
    global buttonSts
    global elapsed_time
    global start_time
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='Assistant service example.')
    parser.add_argument('--language', default=locale_language())
    args = parser.parse_args()
    

    logging.info('Initializing for language %s...', args.language)
    hints = get_hints(args.language)
    client = CloudSpeechClient() #Access google cloudspeechAPI
    if hints:
        logging.info('Say something, e.g. %s.' % ', '.join(hints))
    else:
        logging.info('Say something.')
    text = client.recognize(language_code=args.language,
                            hint_phrases=hints)
    if text is None:
        logging.info('You said nothing.')

    ##Voice command triggers##

    logging.info('You said: "%s"' % text) #Gives feedback in shell about which words google voice heard
    text = text.lower()
    
    #voice command for voice_to_button manipulation
    if 'increase count' in text:
        start_time = time.time()                 
        buttonSts+=1
        ledGreen.on()
        end = time.time()
        aiy.voice.tts.say("Thank you for drinking water")
    else:
        ledGreen.off()
        
    if 'reset' in text:
        buttonSts=0    
        ledRed.on()
        aiy.voice.tts.say("Counter Reset")
    else:
        ledRed.off()
    
#If statement allows you to ask system how many cups of water you've drank today
    if 'how much' in text:
        how_much = "You've drank" + str(buttonSts) + "cups of water today"
        aiy.voice.tts.say(how_much)
    
    if 'goodbye' in text:
        return


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)